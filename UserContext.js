import React from 'react';

// creates a Context Object
const UserContext = React.createContext();

export const UserProvider = UserContext.Provider

export default UserContext;