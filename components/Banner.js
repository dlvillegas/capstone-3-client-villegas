import { useRouter } from "next/router";
import { Row, Col, Jumbotron } from "react-bootstrap";
// Import nextJS Link component for client-side navigation
import BackButton from "./BackButton";
import TravelButton from "../pages/category";

export default function Banner({ data }) {
  const router = useRouter();
  // Destructure the data prop by its properties
  const { title, content } = data;

  return (
    <Row>
      <Col>
        <Jumbotron>
          <h1>{title}</h1>
          <p>{content}</p>
          {router.pathname === "/" ? <category /> : <BackButton />}
        </Jumbotron>
      </Col>
    </Row>
  );
}
