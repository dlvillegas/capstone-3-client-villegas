import React from "react";

export default function Footer() {
  return (
    <div class="container-fluid fixed-bottom footer">
      <div class="row">
        <div>
          <p class="copyright-text text-center">
            Copyright &copy; 2021 All Rights Reserved by
            <a href="https://www.facebook.com/gaming/akiiiigaming">Akiiii</a>.
          </p>
        </div>
      </div>
    </div>
  );
}
