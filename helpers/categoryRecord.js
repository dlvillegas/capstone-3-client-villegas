// Function for converting string data from the enpoints to numbers

export default function categoryRecord(categories, records){

	const combinedCatRec = records.map(record =>{
		const category = categories.find(category => category._id == record.categoryId)

		if(category==undefined){
			record.categoryType = "uncategorized"
			record.categoryName = "uncategorized"
		}else{
			record.categoryName = category.name
			record.categoryType = category.type
		}

		return record
	})

	return combinedCatRec

}