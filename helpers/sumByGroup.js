export default function sumByGroup(labelArray,labelKey,array,key,keySum){

	
	let sumCounts=[]

    array.reduce(function(res, value) {
		if (!res[value.[key]]) {
			res[value.[key]] = { [key]: value.[key], [keySum]: 0, count:0 };
			sumCounts.push(res[value.[key]])
		}
		
		res[value.[key]].[keySum] += value.[keySum]
		res[value.[key]].count += 1
		return res
	}, {})


	
	if(labelArray!=[] & labelKey!= ""){
        
        let labelSumCounts = labelArray.map(label=>{
        
            let foundSumCount = sumCounts.find(sumCount => sumCount.[key]== label.[labelKey])

            if(foundSumCount == undefined){
                label.[keySum] = 0
                label.count = 0
            }else{
                label.[keySum] = foundSumCount.[keySum]
                label.count = foundSumCount.count
            }

            return label
        })
        
        return(labelSumCounts)
	}else{
		return(sumCounts)
	}

	
};
