import React, { useState, useEffect, useRef } from "react";
import { Form, Button, Table, Row, Col, Alert } from "react-bootstrap";
import Router from "next/router";
import Head from "next/head";
import categoryRecord from "../../helpers/categoryRecord";
import moment from "moment";

export default function record() {
  const [categories, setCategories] = useState("");
  const [records, setRecords] = useState("");
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const [type, setType] = useState("all");
  const [action, setAction] = useState("all");
  const [searchString, setSearchString] = useState("");

  const [filteredCategories, setFilCat] = useState([]);
  const [categoryId, setCategoryId] = useState("all");

  const [recordResult, setRecordResult] = useState([]);
  const [recordsOnDate, setRecDate] = useState([]);
  const [recordsOnType, setRecType] = useState([]);
  const [recordsOnCategory, setRecCat] = useState([]);
  const [filteredRecords, setFilRec] = useState([]);

  useEffect(() => {
    fetch(`https://immense-stream-21562.herokuapp.com/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCategories(data.categories);
        setRecords(data.records);
        setStartDate(
          moment(data.currentDate.startDate)
            .startOf("days")
            .format("yyyy-MM-DD")
        );
        setEndDate(
          moment(data.currentDate.endDate).startOf("days").format("yyyy-MM-DD")
        );
      });
  }, []);

  useEffect(() => {
    setCategoryId("all");
    if (categories.length > 0) {
      if (type == "all") {
        setFilCat(categories);
      } else if (type == "income") {
        setFilCat(categories.filter((category) => category.type == "income"));
      } else {
        setFilCat(categories.filter((category) => category.type == "expense"));
      }
    }
  }, [type, categories]);

  useEffect(() => {
    if (action == "all") {
      setRecordResult(records);
    }
  }, [action, records]);

  function search(e) {
    e.preventDefault();
    if (records.length > 0) {
      setRecordResult(
        records.filter((record) => {
          return record.description
            .toLowerCase()
            .includes(searchString.toLowerCase());
        })
      );
    } else {
      setRecordResult([]);
    }
  }

  useEffect(() => {
    console.log(recordResult);
    let dateBefore = moment(startDate).subtract(1, "day");
    let dateAfter = moment(endDate).add(1, "day");

    if (recordResult.length > 0) {
      setRecDate(
        recordResult.filter((record) => {
          let updatedOn = moment(record.updatedOn).startOf("days");
          return moment(updatedOn).isBetween(dateBefore, dateAfter, "day");
        })
      );
    } else {
      setRecDate([]);
    }
  }, [recordResult, startDate, endDate]);

  useEffect(() => {
    if (recordsOnDate.length > 0) {
      if (type == "all") {
        setRecType(recordsOnDate);
      } else if (type == "income") {
        setRecType(recordsOnDate.filter((record) => record.amount > 0));
      } else {
        setRecType(recordsOnDate.filter((record) => record.amount < 0));
      }
    } else {
      setRecType([]);
    }
  }, [recordsOnDate, type]);

  useEffect(() => {
    if (recordsOnType.length > 0) {
      if (categoryId == "all") {
        setRecCat(recordsOnType);
      } else {
        setRecCat(
          recordsOnType.filter((record) => record.categoryId == categoryId)
        );
      }
    } else {
      setRecCat([]);
    }
  }, [recordsOnType, categoryId]);

  useEffect(() => {
    console.log(recordsOnCategory);
    if (recordsOnCategory.length > 0) {
      setFilRec(categoryRecord(filteredCategories, recordsOnCategory));
    } else {
      setFilRec([]);
    }
  }, [recordsOnCategory]);

  return (
    <React.Fragment>
      <Head>
        <title>Search Records</title>
      </Head>

      <Form onSubmit={(e) => search(e)}>
        <Row className="justify-content-md-center my-2">
          <Col xs={3} md={3} lg={3}>
            <Form.Group controlId="searchBy">
              <Form.Control
                as="select"
                required
                onChange={(e) => setAction(e.target.value)}
              >
                <option value="all">All Records</option>
                <option value="search">Search Records</option>
              </Form.Control>
            </Form.Group>
          </Col>
          {action == "all" ? null : (
            <Row>
              <Col xs={8} md={8} lg={8}>
                <Form.Group controlId="searchString">
                  <Form.Control
                    type="string"
                    placeholder="Search..."
                    value={searchString}
                    onChange={(e) => setSearchString(e.target.value)}
                    required
                  />
                </Form.Group>
              </Col>
              <Col xs={4} md={4} lg={4}>
                {searchString.length > 0 ? (
                  <Button variant="primary" type="submit" id="submitBtn">
                    Search
                  </Button>
                ) : (
                  <Button
                    variant="primary"
                    type="submit"
                    id="submitBtn"
                    disabled
                  >
                    Search
                  </Button>
                )}
              </Col>
            </Row>
          )}
        </Row>
        <Row>
          <Col>
            <Row className="justify-content-md-center my-2">
              <Col lg={4}>
                <Form.Group controlId="type">
                  <Row className="justify-content-md-center">
                    <Col lg={3}>
                      <Form.Label>Type:</Form.Label>
                    </Col>
                    <Col>
                      <Form.Control
                        as="select"
                        onChange={(e) => {
                          setType(e.target.value);
                        }}
                        required
                      >
                        <option value="all">All</option>
                        <option value="income">Income</option>
                        <option value="expense">Expense</option>
                      </Form.Control>
                    </Col>
                  </Row>
                </Form.Group>
              </Col>
              <Col lg={7}>
                <Form.Group controlId="categoryId">
                  <Row className="justify-content-md-center">
                    <Col lg={6}>
                      <Form.Label>Category Name:</Form.Label>
                    </Col>
                    <Col lg={6}>
                      <Form.Control
                        as="select"
                        value={categoryId}
                        onChange={(e) => setCategoryId(e.target.value)}
                        required
                      >
                        <option value="all">All</option>
                        {filteredCategories.length == 0
                          ? null
                          : filteredCategories.map((category) => {
                              return (
                                <option value={category._id}>
                                  {category.name}
                                </option>
                              );
                            })}
                      </Form.Control>
                    </Col>
                  </Row>
                </Form.Group>
              </Col>
            </Row>
          </Col>
          <Col>
            <Row>
              <Col>
                <Row className="justify-content-md-center my-2">
                  <Col md="auto">
                    <Form.Label>Date:</Form.Label>
                  </Col>
                  <Col>
                    <Form.Control
                      type="date"
                      dateformat="yyyy-MM-DD"
                      value={startDate}
                      onChange={(e) => setStartDate(e.target.value)}
                    />
                  </Col>
                  <Col md="auto">
                    <Form.Label> to </Form.Label>
                  </Col>
                  <Col>
                    <Form.Control
                      type="date"
                      value={endDate}
                      dateformat="myyyy-MM-DD"
                      onChange={(e) => setEndDate(e.target.value)}
                    />
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
        {filteredRecords.length == 0 ? (
          <Alert variant="info">No Records Found</Alert>
        ) : (
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Type</th>
                <th>Category</th>
                <th>Description</th>
                <th>Amount</th>
                <th>Date</th>
              </tr>
            </thead>

            <tbody>
              {filteredRecords.map((record) => {
                return (
                  <tr key={record._id}>
                    <td>{record.categoryType}</td>
                    <td>{record.categoryName}</td>
                    <td>{record.description}</td>
                    <td>{Math.abs(record.amount)}</td>
                    <td>
                      {moment(record.updatedOn).format("MMMM D, YYYY  hh:mm a")}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </Table>
        )}
      </Form>
      <Button
        variant="success"
        onClick={(e) => Router.push("./record/add")}
        className="my-3"
      >
        Add Record
      </Button>
    </React.Fragment>
  );
}
