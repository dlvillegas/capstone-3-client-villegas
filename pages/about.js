import {
  Form,
  Button,
  Card,
  Row,
  Col,
  Container,
  Jumbotron,
} from "react-bootstrap";
import React, { useState, useEffect, useContext } from "react";

export default function () {
  return (
    <Container className="about my-5">
      <Row>
        <Col>
          <h2 className="aboutTitle">About The App</h2>
        </Col>
        <Col>
          <p className="aboutBody">
            PTK (Pitaka) is a free web-based budget managing app that allows you
            to keep track of your finances.
          </p>
          <p className="aboutBody">
            Built for your daily convenience, PTK has an interface that is
            straightforward and simple to navigate.
          </p>
          <p className="aboutBody">
            It also features graphs and trends that summarize your incomes and
            expenses for quick and trouble-free analysis.
          </p>
          <p className="aboutBody">
            On top of that, you can plan your budget for wiser spending and
            saving money!
          </p>
        </Col>
      </Row>
    </Container>
  );
}
