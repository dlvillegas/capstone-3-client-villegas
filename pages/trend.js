import React, { useState, useEffect } from "react";
import { Form, Button, Table, Row, Col, Alert } from "react-bootstrap";
import Router from "next/router";
import Head from "next/head";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import categoryRecord from "../helpers/categoryRecord";
import moment from "moment";
import { Line } from "react-chartjs-2";

export default function record() {
  const [startDate, setStartDate] = useState(
    moment().subtract(1, "years").format("yyyy-MM-DD")
  );
  const [endDate, setEndDate] = useState(moment().format("yyyy-MM-DD"));
  const [records, setRecords] = useState([]);
  const [updateDates, setupdateDates] = useState(false);
  const [trendData, setTrendData] = useState({});

  useEffect(() => {
    fetch("https://immense-stream-21562.herokuapp.com/api/users/details", {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        let combinedCatReg = categoryRecord(data.categories, data.records);
        console.log(combinedCatReg);
        setRecords(combinedCatReg);
      });
  }, []);

  useEffect(() => {
    if (startDate <= endDate) {
      if (records.length > 0) {
        let dateBefore = moment(startDate).subtract(1, "day");
        let dateAfter = moment(endDate).add(1, "day");

        let filteredRecords = records.filter((record) => {
          let updatedOn = moment(record.updatedOn).startOf("days");
          return moment(updatedOn).isBetween(dateBefore, dateAfter, "day");
        });
        console.log(filteredRecords);

        let oldRecords = records.filter((record) => {
          let updatedOn = moment(record.updatedOn).startOf("days");
          return moment(updatedOn).isSameOrBefore(dateBefore, "day");
        });

        let previousSavings = oldRecords.reduce((total, record) => {
          return total + record.amount;
        }, 0);

        let startMonth = moment(startDate).startOf("month");

        let labelMonth = startMonth;
        let months = [];
        let incomes = [];
        let expenses = [];
        let monthlySavings = [];
        let accumulatedSavings = [];
        while (moment(labelMonth).isSameOrBefore(endDate, "month")) {
          months.push(moment(labelMonth).format("MMM-YYYY"));
          incomes.push(0);
          expenses.push(0);
          monthlySavings.push(0);
          accumulatedSavings.push(previousSavings);
          labelMonth = moment(labelMonth).add(1, "month");
        }

        filteredRecords.map((record) => {
          console.log(record.updatedOn);
          let recordMonth = moment(record.updatedOn).startOf("month");
          console.log(recordMonth);
          let index = moment(recordMonth).diff(
            moment(startMonth),
            "month",
            true
          );
          console.log(index);
          if (record.categoryType == "income") {
            incomes[index] += record.amount;
          } else {
            expenses[index] -= record.amount;
          }
          monthlySavings[index] = incomes[index] - expenses[index];

          let i;
          for (i = index; i < accumulatedSavings.length; i++) {
            console.log(i);
            console.log(accumulatedSavings[i]);
            accumulatedSavings[i] += record.amount;
          }
        });

        console.log(months);
        console.log(incomes);
        console.log(expenses);
        console.log(monthlySavings);

        setTrendData({
          labels: months,
          datasets: [
            {
              label: "Income",
              data: incomes,
              fill: false,
              borderColor: "#498ae6",
            },
            {
              label: "Expenses",
              data: expenses,
              fill: false,
              borderColor: "#f56f36",
            },
            {
              label: "Monthly Savings",
              data: monthlySavings,
              fill: false,
              borderColor: "#47b83d",
            },
            {
              label: "Accumulated Savings",
              data: accumulatedSavings,
              fill: false,
              borderColor: "#0f5209",
            },
          ],
        });
      }
    }
  }, [startDate, endDate, records]);

  return (
    <React.Fragment>
      <Head>
        <title>Monthly Trend</title>
      </Head>
      <Form>
        <Row className="justify-content-center  my-5">
          <Col md="auto">
            <Form.Label>Date:</Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="date"
              value={startDate}
              onChange={(e) => setStartDate(e.target.value)}
            />
          </Col>
          <Col md="auto">
            <Form.Label> to </Form.Label>
          </Col>
          <Col>
            <Form.Control
              type="date"
              value={endDate}
              onChange={(e) => setEndDate(e.target.value)}
            />
          </Col>
        </Row>
        <Line data={trendData} />
      </Form>
    </React.Fragment>
  );
}
