import React, { useEffect, useState } from "react";
import Head from "next/head";
import Router from "next/router";
import { Table, Alert, Container, Button, Col, Row } from "react-bootstrap";
import styles from "../../styles/Home.module.css";
import moment from "moment";
import sumByGroup from "../../helpers/sumByGroup";

export default function category() {
  const [categories, setCategories] = useState([]);
  const [currentRecords, setCurRec] = useState([]);
  const [incomeCategories, setIncCat] = useState([]);
  const [expensesCategories, setExpCat] = useState([]);
  const [incomeRows, setIncomeRows] = useState(null);
  const [expensesRows, setExpensesRows] = useState(null);

  useEffect(() => {
    fetch(`https://immense-stream-21562.herokuapp.com/api/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        setCategories(data.categories);

        let dateBefore = moment(data.currentDate.startDate).subtract(1, "day");
        let dateAfter = moment(data.currentDate.endDate).add(1, "day");

        let filteredRecords = data.records.filter((record) => {
          let updatedOn = moment(record.updatedOn).startOf("days");
          return moment(updatedOn).isBetween(dateBefore, dateAfter, "day");
        });

        setCurRec(filteredRecords);
      });
  }, []);

  useEffect(() => {
    let categoryRecords = sumByGroup(
      categories,
      "_id",
      currentRecords,
      "categoryId",
      "amount"
    );

    setIncCat(categoryRecords.filter((category) => category.type == "income"));
    setExpCat(categoryRecords.filter((category) => category.type == "expense"));
  }, [categories, currentRecords]);

  useEffect(() => {
    setIncomeRows(
      incomeCategories.map((category) => {
        return (
          <tr key={category._id}>
            <td>{category.name}</td>
            <td>{category.amount}</td>
          </tr>
        );
      })
    );
  }, [incomeCategories]);

  useEffect(() => {
    setExpensesRows(
      expensesCategories.map((category) => {
        return (
          <tr key={category._id}>
            <td>{category.name}</td>
            <td>{category.budget}</td>
            <td>{Math.abs(category.amount)}</td>
            <td>{category.amount + category.budget}</td>
          </tr>
        );
      })
    );
  }, [expensesCategories]);

  return (
    <React.Fragment>
      <Head>
        <title> Categories</title>
      </Head>
      <Row className=" my-2">
        <Col>
          <Button
            className="float-left "
            variant="success"
            onClick={() => Router.push("./category/add")}
          >
            Add Category
          </Button>
        </Col>
        <Col>
          <Button
            className="float-left mx-3"
            variant="secondary"
            onClick={() => Router.push("./category/edit")}
          >
            Edit Category
          </Button>
        </Col>
      </Row>
      <h5>Income:</h5>
      {incomeCategories.length == 0 ? (
        <Alert variant="info">You have no categories yet.</Alert>
      ) : (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Category Name</th>
              <th>Current Income</th>
            </tr>
          </thead>

          <tbody>{incomeRows}</tbody>
        </Table>
      )}
      <h5>Expenses:</h5>
      {expensesCategories.length == 0 ? (
        <Alert variant="info">You have no categories under expenses yet.</Alert>
      ) : (
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>Category Name</th>
              <th>Current Budget</th>
              <th>Current Expenses</th>
              <th>Current Savings</th>
            </tr>
          </thead>

          <tbody>{expensesRows}</tbody>
        </Table>
      )}
      <Col>
        <Button
          className="float-right"
          variant="success"
          onClick={() => Router.push("../record/add")}
        >
          Add Record
        </Button>
      </Col>
    </React.Fragment>
  );
}
