import React, { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import Router from "next/router";
import Head from "next/head";
import DropdownButton from "react-bootstrap/DropdownButton";
import Dropdown from "react-bootstrap/Dropdown";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";

export default function newCategory() {
  // const{user} = useContext(UserContext)
  // Form input state hooks
  const [type, setType] = useState("");
  const [name, setName] = useState("");
  const [budget, setBudget] = useState(0);
  const [categories, setCategories] = useState([]);

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // Validate form input whenever email, password1, or password2 is changed
  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (type !== "" && name !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [type, name]);

  function addCategory(e) {
    e.preventDefault();
    console.log(name);
    console.log(type);
    console.log(budget);

    fetch(
      `https://immense-stream-21562.herokuapp.com/api/users/duplicateCategory`,
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          type: type,
          name: name,
        }),
      }
    )
      .then((res) => res.json())
      .then((data) => {
        if (data == true) {
          Swal.fire("Duplicate", `Same category already exist`, "error");
        } else if (data == false) {
          fetch(
            `https://immense-stream-21562.herokuapp.com/api/users/addCategory`,
            {
              method: "PUT",
              headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${localStorage.getItem("token")}`,
              },
              body: JSON.stringify({
                type: type,
                name: name,
                budget: budget,
              }),
            }
          )
            .then((res) => res.json())
            .then((data) => {
              if (data == true) {
                Router.push(".");
              } else {
                Swal.fire("Error", `Something went wrong`, "error");
              }
            });
        } else {
          Swal.fire("Error", `Something went wrong`, "error");
        }
      });
  }

  return (
    <React.Fragment>
      <Head>
        <title>Add Category</title>
      </Head>
      <Form onSubmit={(e) => addCategory(e)}>
        <Form.Group controlId="type">
          <Form.Label>Category Type</Form.Label>
          <Form.Control
            as="select"
            onChange={(e) => setType(e.target.value)}
            required
          >
            <option value="" disabled selected>
              Select Category Type
            </option>
            <option value="income">Income</option>
            <option value="expense">Expense</option>
          </Form.Control>
        </Form.Group>

        <Form.Group controlId="name">
          <Form.Label>Category Name</Form.Label>
          <Form.Control
            type="category"
            placeholder="Enter Category Name"
            value={name}
            onChange={(e) => setName(e.target.value)}
            required
          />
        </Form.Group>
        {type == "expense" ? (
          <Form.Group controlId="budget">
            <Form.Label>Budget</Form.Label>
            <Form.Control
              type="number"
              placeholder="Insert Budget"
              onChange={(e) => setBudget(e.target.value)}
            />
          </Form.Group>
        ) : null}

        {isActive ? (
          <Button variant="primary" type="submit" id="submitBtn">
            Submit
          </Button>
        ) : (
          <Button variant="primary" type="submit" id="submitBtn" disabled>
            Submit
          </Button>
        )}
      </Form>
    </React.Fragment>
  );
}
