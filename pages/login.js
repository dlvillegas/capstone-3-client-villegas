import React, { useState, useEffect, useContext } from "react";
import {
  Form,
  Button,
  Card,
  Row,
  Col,
  Container,
  Jumbotron,
} from "react-bootstrap";
import { GoogleLogin } from "react-google-login";
import Swal from "sweetalert2";
import Router from "next/router";
import Head from "next/head";
import UserContext from "../UserContext";
import View from "../components/View";
import Link from "next/link";

export default function login() {
  return (
    <Container fluid>
      <View title={"Login"}>
        <Row>
          <Col>
            <LoginForm />
          </Col>
        </Row>
      </View>
    </Container>
  );
}

const LoginForm = () => {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  function authenticate(e) {
    e.preventDefault();

    const options = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };

    fetch(`https://immense-stream-21562.herokuapp.com/api/users/login`, options)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          setUser({ id: data._id });
          Router.push("/");
        } else {
          if (data.error === "does-not-exist") {
            Swal.fire("Authentication Failed", "User does not exist.", "error");
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure, try an alternative login procedure",
              "error"
            );
          } else if (data.error === "incorrect-password") {
            Swal.fire(
              "Authentication Failed",
              "password is incorrect",
              "error"
            );
          }
        }
      });
  }

  const authenticateGoogleToken = (response) => {
    const payload = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ tokenId: response.tokenId }),
    };

    fetch(
      `https://immense-stream-21562.herokuapp.com/api/users/verify-google-id-token`,
      payload
    )
      .then((res) => res.json())
      .then((data) => {
        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          setUser({ id: data._id });
          Router.push("/");
        } else {
          if ((data.error = "google-auth-error")) {
            Swal.fire(
              "Google Auth Error",
              "Google authentication procedure failed.",
              "error"
            );
          } else if ((data.error = "login-type-error")) {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure.",
              "error"
            );
          }
        }
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Container className="logIn">
      <Row>
        <Col className="pt-5">
          <h1 className="loginTitle mt-5">PTK</h1>
          <h3>Planning, Tracking, and</h3>
          <h3>Keeping your finances daily.</h3>
        </Col>
        <Col>
          <Card>
            <Card.Header className="text-center">Login </Card.Header>
            <Card.Body>
              <Form onSubmit={(e) => authenticate(e)}>
                <Form.Group controlId="userEmail">
                  <Form.Label className="text-center">Email address</Form.Label>
                  <Form.Control
                    type="email"
                    placeholder="Enter email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                    required
                  />
                </Form.Group>

                <Form.Group controlId="password">
                  <Form.Label className="text-center">Password</Form.Label>
                  <Form.Control
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    required
                  />
                </Form.Group>
                <Row className="justify-content-center px-3 my-1">
                  {isActive ? (
                    <Button className="bg-primary btn-block" type="submit">
                      Submit
                    </Button>
                  ) : (
                    <Button
                      className="bg-primary btn-block px-3 my-1"
                      type="submit"
                      disabled
                    >
                      Submit
                    </Button>
                  )}
                </Row>
              </Form>
              <Link href="/register">Not yet register? Sign up!</Link>

              <GoogleLogin
                clientId="1013780524186-u2455hji1fnhdllscsa1sja67gp312t3.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={authenticateGoogleToken}
                onFailure={authenticateGoogleToken}
                cookiePolicy={"single_host_origin"}
                className="w-100 text-center d-flex justify-content-center mt-2"
              />
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
};
