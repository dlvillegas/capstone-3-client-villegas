import React, { useState, useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import Router from "next/router";
import Head from "next/head";

export default function Register() {
  // Form input state hooks
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // Validate form input whenever email, password1, or password2 is changed
  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (
      firstName != "" &&
      lastName != "" &&
      email != "" &&
      password1 !== "" &&
      password2 !== "" &&
      password2 === password1
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [firstName, lastName, password1, password2]);

  // Function to register user
  function registerUser(e) {
    e.preventDefault();

    // Check for duplicate email in database first
    fetch(`https://immense-stream-21562.herokuapp.com/api/users/email-exists`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        // If no duplicates found
        if (data === false) {
          fetch(`https://immense-stream-21562.herokuapp.com/api/users`, {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              password: password1,
            }),
          })
            .then((res) => res.json())
            .then((data) => {
              if (data === true) {
                Router.push("/login");
              } else {
                Router.push("/error");
              }
            });
        } else {
          Router.push("/error");
        }
      });
  }

  return (
    <Card className="my-5">
      <Card.Header>Register</Card.Header>
      <Card.Body>
        <Form onSubmit={(e) => registerUser(e)}>
          <Form.Group controlId="firstName">
            <Form.Label>First Name</Form.Label>
            <Form.Control
              type="string"
              placeholder="Enter First Name"
              value={firstName}
              onChange={(e) => setFirstName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group controlId="lastName">
            <Form.Label>Last Name</Form.Label>
            <Form.Control
              type="string"
              placeholder="Enter Last Name"
              value={lastName}
              onChange={(e) => setLastName(e.target.value)}
              required
            />
          </Form.Group>
          <Form.Group controlId="userEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control
              type="email"
              placeholder="Enter email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password1">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Password"
              value={password1}
              onChange={(e) => setPassword1(e.target.value)}
              required
            />
          </Form.Group>

          <Form.Group controlId="password2">
            <Form.Label>Verify Password</Form.Label>
            <Form.Control
              type="password"
              placeholder="Verify Password"
              value={password2}
              onChange={(e) => setPassword2(e.target.value)}
              required
            />
          </Form.Group>

          {isActive ? (
            <Button variant="primary" type="submit" id="submitBtn">
              Submit
            </Button>
          ) : (
            <Button variant="primary" type="submit" id="submitBtn" disabled>
              Submit
            </Button>
          )}
        </Form>
      </Card.Body>
    </Card>
  );
}
